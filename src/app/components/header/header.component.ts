import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { LoginService } from 'src/app/services/login.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  userRoleID: any;

  constructor(public loginService: LoginService,
              private router: Router,
              private cookie: CookieService) { 

  }

  ngOnInit(): void {
    console.log(this.cookie.getAll());
    if(typeof this.cookie.get('token') != 'undefined' && this.cookie.get('token').length > 0){
      this.loginService.loginSuccessfully = true;
      this.userRoleID = this.cookie.get('userRoleID');
    }
  }

  CerrarSesion(): void {
    this.loginService.loginSuccessfully = false;
    this.cookie.delete('token');
    this.cookie.delete('userID');
    this.cookie.delete('userRoleID');
    this.router.navigate(["/index"]).then(() =>{
      window.location.reload();
    });
  }
}
