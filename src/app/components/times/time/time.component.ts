import { Component, NgModule, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { ActivityTime } from 'src/app/models/activitytime';
import { TimeService } from 'src/app/services/time.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Activity } from 'src/app/models/activity';

@Component({
  selector: 'app-time',
  templateUrl: './time.component.html',
  styleUrls: ['./time.component.css']
})

export class TimeComponent implements OnInit, OnDestroy{

  form: FormGroup;
  suscription!: Subscription;
  timeObj!: ActivityTime;
  idTime = 0;
  activityTimeCreate! : string;
  activityID = 0;
  idUser? = 0;
  token: any;
  userRoleID: any;
  activityObj!: Activity;

  constructor(private formBuilder: FormBuilder, 
              private timeService: TimeService, 
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private cookie: CookieService,
){ 
    this.form = this.formBuilder.group({
      id: 0,
      nombreActividad: [''],
      tiempoAsignado: ['', [Validators.required, Validators.min(1), Validators.max(8)]],
      fechaTiempoAgregado: ['', [Validators.required]]
    })
  }


  ngOnInit(): void {
    this.token = this.cookie.get('token');
    this.idUser = Number(this.cookie.get('userID'));
    this.userRoleID = Number(this.cookie.get('userRoleID'));

    this.timeService.obtenerTiempo().subscribe(data => {
      this.timeObj = data;
      this.form.patchValue({
        tiempoAsignado: this.timeObj.activityTimeHour,
        fechaTiempoAgregado: this.timeObj.activityTimeDate,
      });
      this.idTime = this.timeObj.activityTimeID!;
      this.activityTimeCreate = this.timeObj.activityTimeDate!;
    });

    this.route.params.subscribe(params => {
      this.activityID = params['idActivity'];
    });
  }

  ngOnDestroy(){
      if(this.suscription){
        this.suscription.unsubscribe();
      }
  }

  crearTiempo(){
    if(this.idTime === 0 || this.idTime === undefined){
      this.agregar();
    }else{
      this.editar();
    }
  }

  agregar(){
    var todayDate = new Date();
    const timeAdd: ActivityTime = {
      activityTimeHour: this.form.get('tiempoAsignado')?.value,
      activityTimeDate: todayDate.toISOString(),
      activityID: this.activityID,
    }

    this.timeService.guardarTiempo(timeAdd, this.token).subscribe(
      data => {
        this.toastr.success('Registro Agregado', 'Se asignó tiempo a esta actividad');
        this.timeService.obtenerTiempos(this.activityID, this.token);
        this.form.reset();
      },
      error => {
        this.toastr.error('Error', error.error);
      }
    );
  }

  editar(){
    const timeModify: ActivityTime = {
      activityTimeHour: this.form.get('tiempoAsignado')?.value,
      activityTimeDate: this.activityTimeCreate,
      activityID: this.activityID,
    }

    this.timeService.actualizarTiempo(this.idTime, timeModify, this.token).subscribe(data => {
      this.toastr.info('Registro Actualizado', 'El tiempo fue modificado');
      this.timeService.obtenerTiempos(this.activityID, this.token);
      this.form.reset();
      this.idTime = 0;
    });
  }
}
