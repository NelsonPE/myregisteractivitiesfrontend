import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';
import { TimeService } from 'src/app/services/time.service';



@Component({
  selector: 'app-list-time',
  templateUrl: './list-time.component.html',
  styleUrls: ['./list-time.component.css']
})
export class ListTimeComponent implements OnInit {
  activityID = 0;  
  idUser? = 0;
  token: any;
  userRoleID: any;

  constructor(public timeService: TimeService,
              public toastr: ToastrService,
              private route: ActivatedRoute,
              private cookie: CookieService) { 

  }

  ngOnInit(): void {
    this.token = this.cookie.get('token');
    this.idUser = Number(this.cookie.get('userID'));
    this.userRoleID = Number(this.cookie.get('userRoleID'));


    this.route.params.subscribe(params => {
      this.activityID = params['idActivity'];
    });

    this.timeService.obtenerTiempos(this.activityID, this.token);
  }

  eliminarTiempo(id: number){
    if(confirm("Esta seguro que desea eliminar el registro?")){
      this.timeService.eliminarTiempo(id, this.token).subscribe(data => {
        this.toastr.warning("Registro eliminado", "Se eliminó tiempo de esta actividad");
        this.timeService.obtenerTiempos(this.activityID, this.token);
      })
    }
  }

  editarTiempo(time: any){
    this.timeService.actualizar(time);
  }
}
