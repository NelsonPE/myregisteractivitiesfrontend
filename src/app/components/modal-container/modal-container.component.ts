import { Component, OnDestroy } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-modal-container',
  template: ''
})
export class ModalContainerComponent implements OnDestroy {
  destroy = new Subject<any>();
  currentDialog = null;

  constructor(
    private modalService: NgbModal,
    route: ActivatedRoute,
    router: Router
  ) {
    /*route.params.pipe(takeUntil(this.destroy)).subscribe(params => {

      this.currentDialog = this.modalService.open(TimeComponent, {centered: true}) as any;

        this.currentDialog.result.then(result => {
            router.navigateByUrl('/activity');
        });
    });*/
  }

  ngOnDestroy() {
    this.destroy.next();
  }
}