import { Component, NgModule, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Activity } from 'src/app/models/activity';
import { User } from 'src/app/models/user';
import { UserActivities } from 'src/app/models/userActivities';
import { ActivityService } from 'src/app/services/activity.service';
import { LoginService } from 'src/app/services/login.service';
import { isThisTypeNode } from 'typescript';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit, OnDestroy{

  form: FormGroup;
  suscription!: Subscription;
  activityObj!: UserActivities;
  activityID = 0;
  idUser? = 0;
  token: string = "";
  userRoleID: number = 0;


  constructor(private formBuilder: FormBuilder, 
              private activityService: ActivityService, 
              public loginService: LoginService,
              private toastr: ToastrService,
              private cookie: CookieService){ 
    this.form = this.formBuilder.group({
      id: 0,
      nombreActividad: ['', [Validators.required, Validators.maxLength(250), Validators.minLength(10)]],
      descripcionActividad: ['', [Validators.required, Validators.maxLength(100), Validators.minLength(5)]],
      userID: 0
    })
   }

  ngOnInit(): void {
    this.token = this.cookie.get('token');
    this.idUser = Number(this.cookie.get('userID'));
    this.userRoleID = Number(this.cookie.get('userRoleID'));

     this.activityService.obtenerActividad().subscribe(data => {
       this.activityObj = data;
       this.form.patchValue({
        nombreActividad: this.activityObj.activityName,
        descripcionActividad: this.activityObj.activityDescription,
       });

       this.activityID = this.activityObj.idActivity!;
     });
  }

  ngOnDestroy(){
    if(this.suscription){
      this.suscription.unsubscribe();
    }
  }

  crearActividad(){
    console.log(this.activityID);
    if(this.activityID === 0 || this.activityID === undefined){
      this.agregar();
    }else{
      this.editar();
    }
  }

  agregar(){
    const activityCreate: Activity = {
      activityName: this.form.get('nombreActividad')?.value,
      activityDescription: this.form.get('descripcionActividad')?.value,
      userID: this.idUser,
    }

    this.activityService.guardarActividad(activityCreate, this.token).subscribe(data => {
      this.toastr.success('Registro Agregado', 'La actividad fue agregada');
      if(this.userRoleID == 1){
        this.activityService.obtenerActividadesAdmin(this.token);
      }else{
        this.activityService.obtenerActividades(this.token, this.idUser);
      }
      this.form.reset();
    });
  }

  editar(){
    const activityModify: Activity = {
      activityID: this.activityID,
      activityName: this.form.get('nombreActividad')?.value,
      activityDescription: this.form.get('descripcionActividad')?.value,
      userID: this.idUser,
    }

    this.activityService.actualizarActividad(this.activityID, activityModify, this.token).subscribe(data => {
      this.toastr.info('Registro Actualizado', 'La actividad fue modificada');
      if(this.userRoleID == 1){
        this.activityService.obtenerActividadesAdmin(this.token);
      }else{
        this.activityService.obtenerActividades(this.token, this.idUser);
      }
      this.form.reset();
      this.activityID = 0;
    });
  }

}
