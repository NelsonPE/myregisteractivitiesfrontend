import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';
import { ActivityService } from 'src/app/services/activity.service';
import { LoginService } from 'src/app/services/login.service';
import { TimeService } from 'src/app/services/time.service';


@Component({
  selector: 'app-list-activity',
  templateUrl: './list-activity.component.html',
  styleUrls: ['./list-activity.component.css']
})
export class ListActivityComponent implements OnInit, OnDestroy {

  userRoleID: number = 0;
  userID: number = 0;
  token: string = "";

  constructor(public activityService: ActivityService,
              public toastr: ToastrService,
              public loginService: LoginService,
              private cookie: CookieService,
              private router: Router,
              private timeService: TimeService) {

  }

  ngOnInit(): void {
    this.token = this.cookie.get('token');
    this.userRoleID = Number(this.cookie.get('userRoleID'));
    this.userID = Number(this.cookie.get('userID'));

    if(this.userRoleID != 1){
      this.activityService.obtenerActividades(this.token, this.userID);
    }
    else{
      this.activityService.obtenerActividadesAdmin(this.token);
    }
      
  }

  ngOnDestroy(): void {

  }

  eliminarActividad(id: number){
    if(confirm("Esta seguro que desea eliminar el registro?")){
      this.activityService.eliminarActividad(id, this.token).subscribe(data => {
        this.toastr.warning("Registro eliminado", "La actividad fue eliminada");
        if(this.userRoleID == 1){
          this.activityService.obtenerActividadesAdmin(this.token);
        }else{
          this.activityService.obtenerActividades(this.token, this.userID);
        }
      })
    }
  }

  editarActividad(activity: any){
    this.activityService.actualizar(activity);
  }
}
