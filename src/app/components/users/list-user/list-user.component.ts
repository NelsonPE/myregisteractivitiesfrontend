import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  token: any;

  constructor(public userService: UserService,
              public toastr: ToastrService,
              private cookie: CookieService) {

  }

  ngOnInit(): void {
    this.token = this.cookie.get('token');
    this.userService.obtenerUsuarios(this.token);

  }

  eliminarUsuario(id: number){
    if(confirm("Esta seguro que desea eliminar el registro?")){
      this.userService.eliminarUsuario(id, this.token).subscribe(data => {
        this.toastr.warning("Registro eliminado", "El usuario fue eliminado");
        this.userService.obtenerUsuarios(this.token);
      })
    }
  }

  editarUsuario(user: any){
    this.userService.actualizar(user);
  }
}
