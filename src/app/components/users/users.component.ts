import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  userRoleID: any;

  constructor(private router: Router,
              private cookie: CookieService) { }

  ngOnInit(): void {
    this.userRoleID = Number(this.cookie.get('userRoleID'));

    if(this.userRoleID != 1){ 
      alert ('Acceso Denegado: Debe tener privilegios de administrador.');
      this.router.navigate(['/index']);
    }
  }

}
