import { Component, NgModule, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { UserrolesService } from 'src/app/services/userroles.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit, OnDestroy{

  form: FormGroup;
  suscription!: Subscription;
  userObj!: User;
  idUser = 0;
  userCreationDate!: string;
  token: any;

  constructor(private formBuilder: FormBuilder, 
              private userService: UserService, 
              public userRoleService: UserrolesService,
              private toastr: ToastrService,
              private cookie: CookieService){ 
    this.form = this.formBuilder.group({
      id: 0,
      nombreCompleto: ['', [Validators.required, Validators.maxLength(250), Validators.minLength(10)]],
      user: ['', [Validators.required, Validators.maxLength(100), Validators.minLength(5)]],
      password: ['', [Validators.required]],
      userRole: ['']
    })
   }

  ngOnInit(): void {
    this.token = this.cookie.get('token');
    this.userRoleService.getRoles();
    this.userService.obtenerUsuario().subscribe(data => {
      this.userObj = data;
      this.form.patchValue({
        nombreCompleto: this.userObj.userCompleteName,
        user: this.userObj.userLogin,
        password: this.userObj.userPassword,
        userRole: this.userObj.userRoleID,
      });
      this.idUser = this.userObj.userID!;
      this.userCreationDate = this.userObj.userCreationDate!;
    });
  }

  ngOnDestroy(){
      if(this.suscription){
        this.suscription.unsubscribe();
      }
  }

  crearUsuario(){
    if(this.idUser === 0 || this.idUser === undefined){
      this.agregar();
    }else{
      this.editar();
    }
  }

  agregar(){
    var todayDate = new Date();
    const userCreate: User = {
      userCompleteName: this.form.get('nombreCompleto')?.value,
      userLogin: this.form.get('user')?.value,
      userPassword: this.form.get('password')?.value,
      userCreationDate: todayDate.toISOString(),
      userRoleID: this.form.get('userRole')?.value,
    }

    this.userService.guardarUsuario(userCreate, this.token).subscribe(
      data => {
        console.log(data);
        this.toastr.success('Registro Agregado', 'El usuario fue agregado');
        this.userService.obtenerUsuarios(this.token);
        this.form.reset();
      },

      error => {
        this.toastr.error('Error', error.error);
        console.log(error.error);
      }
    );
  }

  editar(){
    const userModify: User = {
      userID: this.idUser,
      userCompleteName: this.form.get('nombreCompleto')?.value,
      userLogin: this.form.get('user')?.value,
      userPassword: this.form.get('password')?.value,
      userCreationDate: this.userCreationDate,
      userRoleID: this.form.get('userRole')?.value,
    }

    this.userService.actualizarUsuario(this.idUser, userModify, this.token).subscribe(data => {
      this.toastr.info('Registro Actualizado', 'El usuario fue modificado');
      this.userService.obtenerUsuarios(this.token);
      this.form.reset();
      this.idUser = 0;
    });
  }
}
