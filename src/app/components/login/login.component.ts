import { Component, NgModule, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Login } from 'src/app/models/login';
import { LoginService } from 'src/app/services/login.service';
import { User } from 'src/app/models/user';
import { environment } from 'src/environments/environment';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  
  form: FormGroup;
  userObj!: User;

  constructor(private formBuilder: FormBuilder, 
              private toastr: ToastrService,
              private loginService: LoginService,
              private router: Router,
              private cookie: CookieService) { 
    this.form = this.formBuilder.group({
      user: ['', [Validators.required]],
      password: ['', [Validators.required]]
    })
  }

  ngOnInit(): void {
  }

  IniciarSesion(): void {
      const loginModel : Login = {
        user: this.form.get("user")!.value,
        password: this.form.get("password")!.value
      } 

      this.loginService.login(loginModel).subscribe(
        data => {
          this.cookie.set('token', data.token);
          this.cookie.set('userID', data.userID);
          this.cookie.set('userRoleID', data.userRoleID);
          this.loginService.loginSuccessfully = true;
          this.router.navigate(["/index"]).then(() =>{
            window.location.reload();
          });
        },
        error => {
          this.toastr.error('Error', error.error);
        }
      );
    }
}
