export class UserActivities {
    idActivity?: number;
    activityName!: string;
    activityDescription!: string;
    userName?: string;
}