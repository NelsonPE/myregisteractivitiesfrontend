export class User {
    userID?: number;
    userCompleteName!: string;
    userLogin!: string;
    userPassword!: string;
    userCreationDate?: string;
    userRoleID?: number;
}