export class ActivityTime {
    activityTimeID?: number;
    activityTimeHour!: number;
    activityTimeDate!: string;
    activityID?: number;
}