import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Login } from 'src/app/models/login';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../models/user';
import { GlobalVariable } from '../common/global.common';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  loginSuccessfully = false;
  userLogged!: User;
  myAppUrl = GlobalVariable.BASE_API_URL;
  myApiUrl = 'api/Login/';

  constructor(private http: HttpClient) { }

  login(Login: any): Observable<any> {
    return this.http.post(this.myAppUrl + this.myApiUrl, Login);
  }
}
