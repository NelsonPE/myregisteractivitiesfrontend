import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Activity } from 'src/app/models/activity';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserActivities } from '../models/userActivities';
import { GlobalVariable } from '../common/global.common';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {

  myAppUrl = GlobalVariable.BASE_API_URL;
  myApiUrl = 'api/Activities/';
  
  list!: Activity[];
  listUserActivities!: UserActivities[];
  private actualizarFormulario = new BehaviorSubject<Activity>({} as any);

  constructor(private http: HttpClient) { }
  
  guardarActividad(activity: Activity, token: string): Observable<Activity>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.post<Activity>(this.myAppUrl + this.myApiUrl, activity, {headers: headers});
  }

  obtenerActividades(token: string, userID?: number){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    this.http.get(this.myAppUrl + this.myApiUrl + userID, {headers: headers}).toPromise()
                        .then(data => {
                          this.list = data as Activity[];
                        })
  }

  obtenerActividadesAdmin(token: string){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    this.http.get(this.myAppUrl + this.myApiUrl, {headers: headers}).toPromise()
                        .then(data => {
                          this.listUserActivities = data as UserActivities[];
                        })
  }

  eliminarActividad(id: number, token: string): Observable<Activity>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.delete<Activity>(this.myAppUrl + this.myApiUrl + id, {headers: headers});
  }

  actualizarActividad(id: number, activity: Activity, token: string): Observable<Activity>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })
    
    return this.http.put<Activity>(this.myAppUrl + this.myApiUrl + id, activity, {headers: headers});
  }

  actualizar(activity: any){
    this.actualizarFormulario.next(activity);
  }

  obtenerActividad(): Observable<Activity>{
    return this.actualizarFormulario.asObservable();
  }
}
