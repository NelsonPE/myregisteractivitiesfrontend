import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserRoles } from '../models/userroles';
import { GlobalVariable } from '../common/global.common';

@Injectable({
  providedIn: 'root'
})
export class UserrolesService {
  myAppUrl = GlobalVariable.BASE_API_URL;
  myApiUrl = 'api/UserRoles/';
  list!: UserRoles[];
  
  constructor(private http: HttpClient) { }


  getRoles() {
    this.http.get(this.myAppUrl + this.myApiUrl).toPromise()
              .then(data => {
                this.list = data as UserRoles[];
              })
  }
}
