import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from 'src/app/models/user';
import { BehaviorSubject, Observable } from 'rxjs';
import { GlobalVariable } from '../common/global.common';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  myAppUrl = GlobalVariable.BASE_API_URL;
  myApiUrl = 'api/Users/';
  
  list!: User[];
  private actualizarFormulario = new BehaviorSubject<User>({} as any);

  constructor(private http: HttpClient) { }
  
  guardarUsuario(usuario: User, token: string): Observable<User>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.post<User>(this.myAppUrl + this.myApiUrl, usuario, {headers: headers});
  }

  obtenerUsuarios(token: string){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    this.http.get(this.myAppUrl + this.myApiUrl, {headers: headers}).toPromise()
                        .then(data => {
                          this.list = data as User[];
                        })
  }

  eliminarUsuario(id: number, token: string): Observable<User>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.delete<User>(this.myAppUrl + this.myApiUrl + id, {headers: headers});
  }

  actualizarUsuario(id: number, user: User, token: string): Observable<User>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.put<User>(this.myAppUrl + this.myApiUrl + id, user, {headers: headers});
  }

  actualizar(user: any){
    this.actualizarFormulario.next(user);
  }

  obtenerUsuario(): Observable<User>{
    return this.actualizarFormulario.asObservable();
  }
}