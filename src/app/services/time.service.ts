import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { ActivityTime } from 'src/app/models/activitytime';
import { Activity } from 'src/app/models/activity';
import { BehaviorSubject, Observable } from 'rxjs';
import { GlobalVariable } from '../common/global.common';

@Injectable({
  providedIn: 'root'
})
export class TimeService {

  myAppUrl = GlobalVariable.BASE_API_URL;
  myApiUrl = 'api/ActivityTimes/';
  
  list!: ActivityTime[];
  private actualizarFormulario = new BehaviorSubject<ActivityTime>({} as any);
  private añadirTiempos = new BehaviorSubject<Activity>({} as any);

  constructor(private http: HttpClient) { }
  
  guardarTiempo(activityTime: ActivityTime, token: string): Observable<ActivityTime>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.post<ActivityTime>(this.myAppUrl + this.myApiUrl, activityTime, {headers: headers});
  }

  obtenerTiempos(idActivity: number, token: string){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    this.http.get(this.myAppUrl + this.myApiUrl + idActivity, {headers: headers}).toPromise()
                        .then(data => {
                          this.list = data as ActivityTime[];
                        })
  }

  eliminarTiempo(id: number, token: string): Observable<ActivityTime>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.delete<ActivityTime>(this.myAppUrl + this.myApiUrl + id, {headers: headers});
  }

  actualizarTiempo(id: number, activityTime: ActivityTime, token: string): Observable<ActivityTime>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })

    return this.http.put<ActivityTime>(this.myAppUrl + this.myApiUrl + id, activityTime, {headers: headers});
  }

  actualizar(activityTime: any){
    this.actualizarFormulario.next(activityTime);
  }

  obtenerTiempo(): Observable<ActivityTime>{
    return this.actualizarFormulario.asObservable();
  }

  addTimes(activity: any){
    this.añadirTiempos.next(activity);
  }

  obtenerTiemposParaAñadir(): Observable<Activity>{
    return this.añadirTiempos.asObservable();
  }
}
