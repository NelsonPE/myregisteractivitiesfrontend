import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';

import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersComponent } from './components/users/users.component';
import { UserComponent } from './components/users/user/user.component';
import { ListUserComponent } from './components/users/list-user/list-user.component';
import { FooterComponent } from './components/footer/footer.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './components/header/header.component';
import { ContentComponent } from './components/content/content.component';
import { LoginComponent } from './components/login/login.component';
import { ActivitiesComponent } from './components/activities/activities.component';
import { ActivityComponent } from './components/activities/activity/activity.component';
import { ListActivityComponent } from './components/activities/list-activity/list-activity.component';
import { ModalContainerComponent } from './components/modal-container/modal-container.component';
import { TimesComponent } from './components/times/times.component';
import { TimeComponent } from './components/times/time/time.component';
import { ListTimeComponent } from './components/times/list-time/list-time.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { IndexComponent } from './components/index/index.component';
import { UserrolesComponent } from './components/userroles/userroles.component';
import { AuthGuard } from './guards/auth.guard';




@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    ListUserComponent,
    FooterComponent,
    HeaderComponent,
    ContentComponent,
    LoginComponent,
    ActivitiesComponent,
    ActivityComponent,
    ListActivityComponent,
    ModalContainerComponent,
    TimesComponent,
    TimeComponent,
    ListTimeComponent,
    NotFoundComponent,
    IndexComponent,
    UserrolesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule,
    ToastrModule.forRoot({
      positionClass :'toast-top-right'
    }),
    /*RouterModule.forRoot(routes,{

    })*/
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
